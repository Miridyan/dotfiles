eval (ssh-agent -c) >> /dev/null

# ssh-add ~/.ssh/id_rsa
ssh-add ~/.ssh/id_rsa.github 2> /dev/null
ssh-add ~/.ssh/id_rsa_gitlab 2> /dev/null

set -x fish_greeting
set -x VISUAL nvim
set -x RUST_SRC_PATH '/home/mike/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/lib/rustlib/src/rust'
set -x PATH $PATH /home/mike/.local/bin

abbr n nvim

abbr gcl "git clone"
abbr gsu "git submodule update --recursive"
abbr gsi "git submodule update --init --recursive"
abbr gco "git checkout"
abbr gpl "git pull"
abbr gps "git push"
abbr gpf "git push -f"
abbr gad "git add"
abbr gaa "git add ."
abbr gcm "git commit -m"
abbr gca "git commit --amend"
abbr grb "git rebase"
abbr gst "git status"
